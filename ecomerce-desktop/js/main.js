$( document ).ready(function() {
    
// Chamada das informações dos produtos da vitrine

var jqxhr = $.getJSON( "js/json_data.json", function(obj) {
  	
	$.each(obj, function(key, value){

		var vitrine =  $(".vitrine ul.vitrine-list");			

		vitrine.append("<li class='vitrine-item'><img src='img/img-product.jpg' alt='bata bordada'/><div class='vitrine-tit'>"+value.name+"</div><div class='vitrine-price'><div class='price'><div class='price-box'><div class='price-old'>"+"R$ "+value.oldprice+"</div><div class='price-full'>"+"R$ "+value.newprice+"</div></div></div><div class='vitrine-checkout'></div></div></li>");			
		
	});
	
})
  .done(function() {
    console.log( "second success" );
  })
  .fail(function() {
    console.log( "error" );
  })
  .always(function() {
    console.log( "complete" );
  });
 

});
